FROM openjdk:11-jre-slim

WORKDIR /app

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar

COPY deployment/env.yml env.yml
EXPOSE 8080

CMD ["java", "-jar", "app.jar", "--spring.config.location=classpath:/application.yaml,file:./env.yml"]



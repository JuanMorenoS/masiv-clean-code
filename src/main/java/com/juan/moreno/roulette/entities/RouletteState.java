package com.juan.moreno.roulette.entities;

public enum RouletteState {

	OPEN,

	CLOSE,

	FINISHED
}

package com.juan.moreno.roulette.entities;

import java.math.BigDecimal;

public class NumberBid extends Bid {

	private final int number;

	public NumberBid(int number, BigDecimal value, String userId) {
		super(value, userId);
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	@Override
	public BigDecimal getPrice() {
		return BigDecimal.valueOf(5).multiply(getValue());
	}

	@Override
	public boolean isWinner(int number) {
		return number == this.number;
	}
}

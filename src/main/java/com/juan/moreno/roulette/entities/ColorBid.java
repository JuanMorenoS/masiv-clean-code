package com.juan.moreno.roulette.entities;

import java.math.BigDecimal;

public class ColorBid extends Bid {

	private final RouletteColor color;

	public ColorBid(RouletteColor color, BigDecimal value, String userId) {
		super(value, userId);
		this.color = color;
	}

	public RouletteColor getColor() {
		return color;
	}

	@Override
	public BigDecimal getPrice() {
		return BigDecimal.valueOf(1.8).multiply(getValue());
	}

	@Override
	public boolean isWinner(int number) {
		RouletteColor expected = number % 2 == 0 ? RouletteColor.RED : RouletteColor.BLACK;
		return expected.equals(color);
	}
}

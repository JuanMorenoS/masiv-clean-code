package com.juan.moreno.roulette.entities;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.google.common.collect.ImmutableList;
import com.juan.moreno.roulette.entities.exceptions.RouletteException;


@Document
public class Roulette {

	private static final BigDecimal MAX_VALUE = new BigDecimal(10_000);

	@Id
	private final String id;

	private final RouletteState state;

	private final List<Bid> bids;

	private final Integer winningNumber;

	public Roulette(String id, RouletteState state, List<Bid> bids, Integer winningNumber) {
		this.id = id;
		this.state = state;
		this.bids = ImmutableList.copyOf(bids);
		this.winningNumber = winningNumber;
	}

	public String getId() {
		return id;
	}

	public RouletteState getState() {
		return state;
	}

	public List<Bid> getBids() {
		return bids;
	}

	public Roulette addBid(Bid bid) {
		if (state == RouletteState.OPEN) {
			if (isValidBidValue(bid)) {

				ArrayList<Bid> bids = new ArrayList<>(this.bids);
				bids.add(bid);

				return new Roulette(id, state, bids, null);
			} else {
				throw new RouletteException("The bid exceded the maximum: " + MAX_VALUE);
			}
		}
		throw new RouletteException("The roulette is not open to accept bids. Please open the roulette");
	}

	private boolean isValidBidValue(Bid bid) {
		return bid.getValue().compareTo(MAX_VALUE) <= 0 &&
				bid.getValue().compareTo(BigDecimal.ZERO) > 0;
	}


	public Roulette openRoulette() {
		switch (state) {
			case OPEN:
				throw new RouletteException("The roulette is already opened");
			case FINISHED:
				throw new RouletteException("This roulette is ended");
		}
		return new Roulette(id, RouletteState.OPEN, bids, null);
	}

	public Roulette closeRoulette() {
		if (state == RouletteState.OPEN) {
			Random r = new Random();
			int low = 0;
			int high = 36;
			int result = r.nextInt(high - low) + low;

			return new Roulette(id, RouletteState.FINISHED, bids, result);
		}
		throw new RouletteException("You can't close an closed or finished roulette");
	}

	public BidResult getResult() {

		if (state == RouletteState.FINISHED) {
			java.util.List<Bid> winners = bids.stream()
					.filter(x -> x.isWinner(winningNumber))
					.collect(Collectors.toList());
			return new BidResult(winners, winningNumber);
		}
		return null;
	}

}

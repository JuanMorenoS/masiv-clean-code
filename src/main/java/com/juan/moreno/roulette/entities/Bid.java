package com.juan.moreno.roulette.entities;

import java.math.BigDecimal;

public abstract class Bid {

	private final BigDecimal value;

	private final String userId;

	protected Bid(BigDecimal value, String userId) {
		this.value = value;
		this.userId = userId;
	}

	public BigDecimal getValue() {
		return value;
	}

	public abstract BigDecimal getPrice();

	public abstract boolean isWinner(int value);

	public String getUserId() {
		return userId;
	}
}

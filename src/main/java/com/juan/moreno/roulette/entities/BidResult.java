package com.juan.moreno.roulette.entities;

import java.util.List;

public class BidResult {

	private final List<Bid> winners;

	private final int winningNumber;

	public BidResult(List<Bid> winners, int winningNumber) {
		this.winners = winners;
		this.winningNumber = winningNumber;
	}

	public List<Bid> getWinners() {
		return winners;
	}

	public int getWinningNumber() {
		return winningNumber;
	}
}

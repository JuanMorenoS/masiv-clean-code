package com.juan.moreno.roulette.entities.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class RouletteException extends RuntimeException {

	public RouletteException(String message) {
		super(message);
	}
}

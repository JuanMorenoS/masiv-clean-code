package com.juan.moreno.roulette.controller.dto;

import java.math.BigDecimal;

import com.juan.moreno.roulette.entities.ColorBid;
import com.juan.moreno.roulette.entities.RouletteColor;

public class ColorBidDto {

	private RouletteColor color;
	private BigDecimal value;

	public ColorBidDto(RouletteColor color, BigDecimal value) {
		this.color = color;
		this.value = value;
	}


	public ColorBid toBid(String userId) {
		return new ColorBid(color, value, userId);
	}

	public RouletteColor getColor() {
		return color;
	}

	public void setColor(RouletteColor color) {
		this.color = color;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}
}

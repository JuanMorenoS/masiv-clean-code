package com.juan.moreno.roulette.controller.dto;

import java.math.BigDecimal;

import com.juan.moreno.roulette.entities.NumberBid;

public class NumberBidDto {

	private final int number;
	private final BigDecimal value;

	public NumberBidDto(int number, BigDecimal value) {
		this.number = number;
		this.value = value;
	}

	public NumberBid toBid(String userId) {
		return new NumberBid(number, value, userId);
	}

	public int getNumber() {
		return number;
	}

	public BigDecimal getValue() {
		return value;
	}
}

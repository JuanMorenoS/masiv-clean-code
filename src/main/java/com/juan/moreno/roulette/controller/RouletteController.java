package com.juan.moreno.roulette.controller;

import com.juan.moreno.roulette.controller.dto.ColorBidDto;
import com.juan.moreno.roulette.controller.dto.NumberBidDto;
import com.juan.moreno.roulette.entities.BidResult;
import com.juan.moreno.roulette.entities.Roulette;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RouletteController {

	Mono<String> createRoulette();

	Mono<String> openRoulette(String id);

	Mono<BidResult> closeRoulette(String id);

	Mono<String> colorBidForRoulette(String rouletteId, String userId, ColorBidDto bid);

	Mono<String> numberBidForRoulette(String rouletteId, String userId, NumberBidDto bid);

	Flux<Roulette> getAllRoulette();

}

package com.juan.moreno.roulette.controller;

import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import com.juan.moreno.roulette.entities.exceptions.RouletteException;
import reactor.core.publisher.Mono;

@Component
@Order(-2)
public class RestExceptionHandler implements ErrorWebExceptionHandler {


	@Override
	public Mono<Void> handle(ServerWebExchange serverWebExchange, Throwable throwable) {
		DataBufferFactory bufferFactory = serverWebExchange.getResponse().bufferFactory();
		serverWebExchange.getResponse().getHeaders().setContentType(MediaType.TEXT_PLAIN);

		DataBuffer dataBuffer;
		if (throwable instanceof RouletteException) {
			serverWebExchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
			dataBuffer = bufferFactory.wrap(throwable.getMessage().getBytes());
		} else {
			serverWebExchange.getResponse().setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
			dataBuffer = bufferFactory.wrap("Unknown error".getBytes());
		}

		return serverWebExchange.getResponse().writeWith(Mono.just(dataBuffer));
	}
}

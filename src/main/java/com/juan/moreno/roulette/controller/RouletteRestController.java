package com.juan.moreno.roulette.controller;

import java.util.ArrayList;
import java.util.UUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.juan.moreno.roulette.controller.dto.ColorBidDto;
import com.juan.moreno.roulette.controller.dto.NumberBidDto;
import com.juan.moreno.roulette.entities.BidResult;
import com.juan.moreno.roulette.entities.Roulette;
import com.juan.moreno.roulette.entities.exceptions.RouletteException;
import com.juan.moreno.roulette.entities.RouletteState;
import com.juan.moreno.roulette.repositories.RouletteRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/api/v1/roulettes")
public class RouletteRestController implements RouletteController {

	private final RouletteRepository repository;

	public RouletteRestController(RouletteRepository repository) {
		this.repository = repository;
	}

	@Override
	@PostMapping
	public Mono<String> createRoulette() {
		Roulette roulette = new Roulette(UUID.randomUUID().toString(), RouletteState.CLOSE, new ArrayList<>(), null);
		return repository.save(roulette).map(Roulette::getId);
	}

	@Override
	@PutMapping("/{id}/open")
	public Mono<String> openRoulette(@PathVariable("id") String id) {
		return repository.findById(id)
				.map(Roulette::openRoulette)
				.flatMap(repository::save)
				.map(x -> "Ok")
				.onErrorResume(RouletteException.class, e -> Mono.just(e.getMessage()));
	}

	@Override
	@PutMapping("/{id}/close")
	public Mono<BidResult> closeRoulette(@PathVariable("id") String id) {
		return repository.findById(id)
				.map(Roulette::closeRoulette)
				.flatMap(repository::save)
				.map(Roulette::getResult);
	}

	@Override
	@PostMapping("{rouletteId}/bids/color")
	public Mono<String> colorBidForRoulette(@PathVariable("rouletteId") String rouletteId,
											@RequestHeader("userId") String userId,
											@RequestBody ColorBidDto bid) {
		return repository.findById(rouletteId)
				.map(x -> x.addBid(bid.toBid(userId)))
				.flatMap(repository::save)
				.map(x -> "Ok")
				.onErrorResume(RouletteException.class, e -> Mono.just(e.getMessage()));
	}

	@Override
	@PostMapping("{rouletteId}/bids/number")
	public Mono<String> numberBidForRoulette(@PathVariable("rouletteId") String rouletteId,
											 @RequestHeader("userId") String userId,
											 @RequestBody NumberBidDto bid) {
		return repository.findById(rouletteId)
				.map(x -> x.addBid(bid.toBid(userId)))
				.flatMap(repository::save)
				.map(x -> "Ok")
				.onErrorResume(RouletteException.class, e -> Mono.just(e.getMessage()));
	}

	@Override
	@GetMapping
	public Flux<Roulette> getAllRoulette() {
		return repository.findAll();
	}
}

package com.juan.moreno.roulette.repositories;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import com.juan.moreno.roulette.entities.Roulette;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RouletteRepository extends ReactiveCrudRepository<Roulette, String> {

	Mono<Roulette> findById(String id);

	Mono<Roulette> save(Roulette roulette);

	Flux<Roulette> findAll();
}

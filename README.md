# Roulette / Clean Code

## Style

The service is made with webflux, spring's framework for reactive programming. 
Mostly everything is made with functional programming, 
in order to make the objects immutable and not [anemic](https://www.martinfowler.com/bliki/AnemicDomainModel.html).


## End-points

POST /api/v1/roulettes create a roulette

PUT /api/v1/roulettes/{rouletteId}/open open a roulette

PUT /api/v1/roulettes/{rouletteId}/close close a roulette

POST /api/v1/roulettes/{rouletteId}/bids/color add a color bid to a roulette. NOTE: userId is mandatory HEADER 
```json
{
    "value":"10000", 
    "color":"BLACK"
}
```

POST /api/v1/roulettes/{rouletteId}/bids/number add a number bid to a roulette. NOTE: userId is mandatory HEADER 
```json
{
    "value":"10000",
    "number":"8"
}
```

for the bids the max value is 10000, and the min is 1

GET /api/v1/roulettes return all the roulettes with the state 

```json
[
  {
        "id": "1a80d33b-bfbe-4d81-a22b-58167120f6f7",
        "state": "FINISHED",
        "bids": [
            {
                "value": 10000,
                "userId": "1234",
                "color": "BLACK",
                "price": 18000.0
            }
        ],
        "result": {
            "winners": [],
            "winningNumber": 14
        }
    },
  {
        "id": "693d31f6-d42d-465f-8702-3d44b30b9533",
        "state": "CLOSE",
        "bids": [],
        "result": null
    },
    {
        "id": "163e058a-2647-4884-9a09-7990fd05cda5",
        "state": "FINISHED",
        "bids": [
                    {       
                        "value": 1,
                        "userId": "1234",
                        "color": "RED",
                        "price": 18000.0
                    },
                    {
                        "value": 10000,
                        "userId": "1234",
                        "number": 1,
                        "price": 50000
                    }
              ]
    }
]
```



## Technologies

* [Spring Boot](https://spring.io/)
* [Web Flux](https://www.baeldung.com/spring-webflux)
* [MongoDB](https://www.mongodb.com/)

#### References
    https://www.martinfowler.com/bliki/AnemicDomainModel.html
    https://en.wikipedia.org/wiki/Reactive_programming
